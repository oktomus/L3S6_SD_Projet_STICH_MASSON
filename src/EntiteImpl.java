/**
 * Joseph Stich
 * Kevin Masson
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */

import java.rmi.* ;
import java.rmi.server.UnicastRemoteObject ;
import java.util.concurrent.locks.*;

/** Classe abstraite de l'entité
 */
public abstract class EntiteImpl
  extends UnicastRemoteObject
  implements Entite{

  /** Stock courant de l'entité */
  protected int stock;

  private static final long serialVersionUID = 42L;
  
  /** Configuration du jeu */
  protected Configuration config;
  
  /** Mécanismes de synchronisation */
  protected Lock lock;
  protected Condition attenteInteraction;
  protected int action;
  protected int id;

  /** Permet de savoir si l'entité peut se terminer ou non */
  protected boolean stop;

  /** L'adresse de l'entité (Nom de l'objet compris) */
  protected String rmiAddr;

  /** Constructeur par défaut
   * @throws java.rmi.RemoteException
   *
   * @param serverAddr l'adresse rmi de la forme server:port
   * @param id      l'id unique de l'entité
   */
  public EntiteImpl(String serverAddr, int id) throws RemoteException{
    lock = new ReentrantLock();
    attenteInteraction = lock.newCondition();

    this.id = id;
    this.rmiAddr = serverAddr + "/" + type() + id;
    stop = false;
  }

  @Override
  public int getStock() throws RemoteException{
    return stock;
  }

  @Override
  public void setStock(int vStock) throws RemoteException{
      this.stock = vStock;
  }
  
  @Override
  public void init(Configuration vConfig) throws RemoteException {
    this.config = vConfig;
  }

  @Override
  public void stop(){
    lock.lock();
    try{
      this.stop = true;
      attenteInteraction.signal();
    }finally{
      lock.unlock();
    }
  }
  
  @Override
  public void lancerTour() throws RemoteException{
    lock.lock();
    try{
      action = 2;
      attenteInteraction.signal();
    }finally{
      lock.unlock();
    }
  }

  @Override
  public void signalerFinTour() throws RemoteException{
    lock.lock();
    try{
      action = 1;
      attenteInteraction.signal();
    }finally{
      lock.unlock();
    }

  }


  @Override
  public int reduireStock(int vQuantiteRecolte){
    //le nouveau stock hypothétique
    int nouveauStock = stock - vQuantiteRecolte;
    int stockDifference = 0;

    //on empêche d'aller dans le négatif
    if(nouveauStock < 0) {
      stockDifference = stock;
      stock = 0;
    } else {
      stockDifference = vQuantiteRecolte;
      stock = nouveauStock;
    }
    return stockDifference;
  }

  /** Retourne l'adresse RMI de l'entité */
  public String getAddr() { 
    return rmiAddr;
  }

  /** Déroulement d'un tour de jeu de l'entité
   *
   * @return  Une chaine devrivant ce que l'entite a fait
   */
  protected abstract String jouer() throws RemoteException;

  protected abstract String type();

  public String nom() throws RemoteException{
    return type() + this.id;
  }

  @Override
  public String etat() throws RemoteException {
    return nom() + " : Stock (" + this.getStock() + ")";
  }

  public void demarrerJeu(Jeu j){
    try{
      while(!stop) {
        lock.lock();
        try {
          attenteInteraction.await();
          if(stop) break;
          if(action == 1) {
            if(!j.estFini()) System.out.println("** " + j.etat());
            //Afficher état jeu
          } else {
            System.out.println("------------------------");
            System.out.println("C'est à vous de jouer !");
            String actionDesc = jouer();
            action = 1;
            System.out.println(actionDesc);
            j.signalerFinAction(nom() + ", " + actionDesc);
            System.out.println("------------------------");
          }
        } catch (InterruptedException ex) {
          System.out.println(ex);
          System.exit(1);
        } finally {
          lock.lock();
        }

      }

      System.out.println("**** " + j.etat());

    }
    catch (RemoteException re) {
      System.out.println(re);
      System.out.println("Une erreur est survenue pendant le déroulement du jeu.");
    }

  }

}
