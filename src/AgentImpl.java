/**
 * Joseph Stich
 * Kevin Masson
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */

import java.rmi.server.UnicastRemoteObject ;
import java.rmi.* ;
import java.net.* ;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Random;

/** Implémentation d'un agent
 */
public class AgentImpl
  extends EntiteImpl
  implements Agent{
  
  private static final long serialVersionUID = 44L;

	/** Différentes personnalités d'un agent*/
	private static final int RECOLTEUSE = 1;
  private static final int VOLEUSE = 2;
	/** Personnalité de l'agent*/
	int personnalite;

  /** Liste des producteurs du jeu */
  ArrayList<Producteur> producteurs;
  /** Liste des agents du jeu */
  ArrayList<Agent> agents;
	/** Liste des agents connus */
  ArrayList<Agent> agentsConnus;
  /** Etat d'observation */
	boolean observation;

  public AgentImpl(String serverAddr, int id) throws RemoteException{
    super(serverAddr, id);
    producteurs = new ArrayList<>();
    agents = new ArrayList<>();
		agentsConnus = new ArrayList<>();
		observation = false;

		Random choix = new Random();
  	if(choix.nextInt(2) % 2 == 0) {
			System.out.println("---- Personnalité [RECOLTEUSE]");
			personnalite = RECOLTEUSE;
		} else {
			System.out.println("---- Personnalité [VOLEUSE]");
			personnalite = VOLEUSE;
		}
  }

  
  @Override
  public int recolter(Producteur vProd) throws RemoteException{
    int recolte = vProd.reduireStock(config.getParam("quantiteRecolte"));
    stock += recolte;
    return recolte;
  }

  @Override
  public int voler(Agent vAgent) throws RemoteException{
    System.out.println("-- Tentative de vol (" + vAgent.nom() + ") ...");
		int vol;

    if(vAgent.getEtat()) {
			vol = vAgent.voler(this);
			vol = -1; //valeur d'erreur
			System.out.println("-- Vol contré !");
		} else {
			vol = vAgent.reduireStock(config.getParam("quantiteRecolte"));
			stock += vol;
    	System.out.println("-- Stock volé : +" + vol);
		}

    return vol;
  }

  @Override
  protected String jouer() throws RemoteException{

    System.out.println("Ressources en stock: " + stock);

	  boolean victoireStock = config.getParam("conditionVictoire") == 2 || 
	    config.getParam("conditionVictoire") == 3;

		if(personnalite == RECOLTEUSE) {
			//on réinitialise l'état d'observation
			observation = false;

		  //on parcourt les producteurs pour récuperer celui avec le plus grand stock
		  Producteur pChoisi = producteurs.get(0);   
		  System.out.print("Producteurs: ");
		  for (Producteur p : producteurs) {
		    System.out.print(p.nom() + "[" + p.getStock() + "] ");
		    if(p.getStock() > pChoisi.getStock())
		      pChoisi = p;
		  }
		  System.out.println();

	    //on regarde si le stock du pChoisi permet de gagner
		  if(victoireStock && 
		      pChoisi.getStock() + stock >= config.getParam("stockVictoireAgent")) {
		    int recolte = this.recolter(pChoisi);
		    return "Recolte de " + recolte + " au près de " + pChoisi.nom() + ".";
      }

	    //on fixe un seuil pour ne pas épuiser la ressource
    	int seuil = 30;

			if(pChoisi.getStock() < seuil) {
				//on passe son tour en se mettant en état d'observation
				observation = true;
				return "En état d'observation...";
			}

			//sinon, on récolte auprès du producteur
			int recolte = this.recolter(pChoisi);
      return "Recolte de " + recolte + " au près de " + pChoisi.nom() + ".";
		}

		if(personnalite == VOLEUSE) {

			//on récupère les informations de l'observation
			if(observation) {
				System.out.print("Agents observés: ");
				if(agents.size() < 1) {
					System.out.println("Vous êtes le seul !");
					//on change de personnalité et on passe son tour
					personnalite = RECOLTEUSE;
					observation = true;
					return "En état d'observation... (Changement de personnalité : RECOLTEUSE)";
				} else{
					//on stocke la liste des agents et leur état du tour précédent
					observation = false;
					agentsConnus = new ArrayList<>();
				  for (Agent a : agents) {
						agentsConnus.add(a);
				    System.out.print(a.nom() + "[" + a.getStock() + "] ");
				  }
				  System.out.println();
				}
				System.out.println();
			}
	
			//si on ne connait pas encore d'agents, alors on se met en observation
			if(agentsConnus.size() == 0) {
				observation = true;
				return "En état d'observation...";
			}

			Agent aChoisi = agentsConnus.get(0);
			for(Agent a : agentsConnus) {
				//on stocke l'agent avec le plus grand stock
				if(a.getStock() > aChoisi.getStock()) aChoisi = a;

				//on prend l'agent correspondant dans la liste des agents
				for(Agent ag : agents) {
					if(ag.nom() == a.nom()) {
						aChoisi = ag;
					}
					break;
				}		
			}

			//on regarde si on peut gagner en volant
			if(victoireStock &&
				aChoisi.getStock() + stock >= config.getParam("stockVictoireAgent")) {
					int vol = this.voler(aChoisi);
					if(vol < 0) {
						return "Vol contré auprès de " + aChoisi.nom() + ". Perte de " + vol + ".";
					}
					return "Vol de " + vol + " auprès de " + aChoisi.nom() + ".";
				}

			//on vole l'agent avec le plus grand stock
			int vol = this.voler(aChoisi);
			if(vol == 0) {
				//on change de personnalité et on passe son tour
				personnalite = RECOLTEUSE;
				observation = true;
				return "En état d'observation... (Changement de personnalité : RECOLTEUSE)";
			}
			
			if(vol < 0) 
				return "Vol contré auprès de " + aChoisi.nom() + ". Perte de " + vol + ".";				
			
      return "Vol de " + vol + " auprès de " + aChoisi.nom() + ".";
		}

    return "Rien fait..";
  }


  
  public String type(){
    return Entite.Type.AGENT.toString();
  }

	public boolean getEtat(){
		return observation;
	}
  
  @Override
  public void connexionProducteurs(ArrayList<String> producteurs) throws RemoteException
    , MalformedURLException
    , NotBoundException{
      this.producteurs = new ArrayList<Producteur>();

      for(String addr : producteurs){
        try{
          Producteur prodDistant = (Producteur) Naming.lookup("rmi://" + addr);
          this.producteurs.add(prodDistant);
        }catch(Exception e){
          System.out.println("Impossible de se connecter au producteur " + addr);
          throw e;
        }
      }
  }

  @Override
  public void connexionAgents(ArrayList<String> agents) throws RemoteException
    , MalformedURLException
    , NotBoundException{
      this.agents = new ArrayList<Agent>();

      for(String addr : agents){
        try{
          Agent agentDistant = (Agent) Naming.lookup("rmi://" + addr);

          this.agents.add(agentDistant);
        }catch(Exception e){
          System.out.println("Impossible de se connecter à l'agent " + addr);
          throw e;
        }
      }
  }

  public static void main(String [] args){

    String rmiJeu, rmiAgent;

    if(args.length != 2){
      System.out.println("USAGE : java AgentImpl adrJeu:portJeu adrAgent:portAgent");
      Utils.stop();
    }	

    rmiJeu = args[0];
    rmiAgent = args[1];

    // Recuperation jeu
    Jeu j = null;
    j = Utils.lookupJeuOrFail(rmiJeu);
    
    int avId = 0;
    AgentImpl ag = null;

    // Creation agent
    try{
      avId = j.getAvailableId();
      ag = new AgentImpl(rmiAgent, avId);
    }catch(RemoteException re){
      System.out.println(re);
      System.out.println("Impossible d'appeler le jeu");
      Utils.stop();
    }

    // Bind agent
    Utils.bindEntiteOrFail(ag);

    // Enregistrement de l'agent aupres du jeu
    try{
      j.ajouterAgent(ag.getAddr());
    }catch(Exception re){
      System.out.println(re);
      System.out.println("Impossible d'ajouter l'agent au jeu.");
      Utils.unbindObjectOrFail(rmiAgent);
      Utils.stop();
    }
    
    System.out.println("Connexion au jeu réussie.");
    System.out.println();

    ag.demarrerJeu(j);
    Utils.unbindObjectOrFail(ag.rmiAddr);

    System.exit(0);

  }


}
