/**
 * Joseph Stich
 * Kevin Masson
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */

import java.net.MalformedURLException;
import java.rmi.* ;
import java.util.logging.Level;
import java.util.logging.Logger;

/** Implémentation d'un producteur
 */
public class ProducteurImpl
  extends EntiteImpl
  implements Producteur{
  
  private static final long serialVersionUID = 43L;

  public ProducteurImpl(String serverAddr, int id) throws RemoteException{
    super(serverAddr, id);
  }

  /** Méthode appelée pendant le tour du producteur pour produire du stock
   * (basée sur la configuration du jeu)
  */
  public void produire(){

    int stockProdMax = config.getParam("stockProdMax");

    //cf wikis/configuration, partie "Producteurs"
    switch(config.getParam("evolutionProduction")) {
      case 1: 

        stock += (stock/2) + 1;
        if(stockProdMax > 0 && stock > stockProdMax) stock = stockProdMax;

        break;

      case 2:

        int quantitéProduction = config.getParam("quantiteProduction");

        stock += quantitéProduction;
        if(stockProdMax > 0 && stock > stockProdMax) stock = stockProdMax;
        break;

      case 3:
        System.out.println("ProducteurImpl.produire() : case 3 | Not implemented yet");
        break;
      default:
        throw new UnsupportedOperationException("Configuration inconnue : evolutionProduction");
    }
  }

  @Override
  public void init(Configuration vConf) throws RemoteException{
    super.init(vConf);
    this.stock = config.getParam("stockProdInitial");
  }

  @Override
  protected String jouer() throws RemoteException{
      System.out.println("Ressources en stock: " + stock);
      int stockActuel = stock;
      this.produire();
      return "Production de " + (stock - stockActuel) + " ressource(s).";
  }
  
  public String type(){
    return Entite.Type.PRODUCTEUR.toString();
  }

  public static void main(String [] args) {

    String rmiJeu, rmiProd;

    if(args.length != 2){
      System.out.println("USAGE : java ProducteurImpl adrJeu:portJeu adrProd:portProd");
      Utils.stop();
    }	

    rmiJeu = args[0];
    rmiProd = args[1];

    // Recuperation jeu
    Jeu j = null;
    j = Utils.lookupJeuOrFail(rmiJeu);
    
    int avId = 0;
    ProducteurImpl pr = null;

    // Creation producteur
    try{
      avId = j.getAvailableId();
      pr = new ProducteurImpl(rmiProd, avId);
    }catch(RemoteException re){
      System.out.println(re);
      System.out.println("Impossible d'appeler le jeu");
      Utils.stop();
    }

    // Bind agent
    Utils.bindEntiteOrFail(pr);

    // Enregistrement du prod aupres du jeu
    try{
      j.ajouterProducteur(pr.getAddr());
    }catch(Exception re){
      System.out.println(re);
      System.out.println("Impossible d'ajouter le producteur au jeu.");
      Utils.unbindObjectOrFail(rmiProd);
      Utils.stop();
    }
    
    System.out.println("Connexion au jeu réussie.");
    System.out.println();

    pr.demarrerJeu(j);
    Utils.unbindObjectOrFail(pr.rmiAddr);

    System.exit(0);


  }

}

