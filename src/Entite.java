/**
 * Joseph Stich
 * Kevin Masson
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */

import java.rmi.* ;
/** Interface de contrôle des entités
 */
public interface Entite extends Remote{

  /** Type de l'entité */
  public enum Type{
    AGENT("Agent"),
    PRODUCTEUR("Producteur");

    private String toString;

    private Type(String name){
      this.toString = name;
    }

    /** Permet de recupérer le type sous forme de chaine */
    public String toString(){
      return toString;
    }
      
  }

  /** Retourne le stock courant
   *
   * @return    Le stock de l'entite
   * @throws java.rmi.RemoteException
   */
  public int getStock() throws RemoteException;

  /** Actualise la valeur du stock courant
   * 
     * @param vStock : nouvelle valeur du stock
   * @throws java.rmi.RemoteException
   */
  public void setStock(int vStock) throws RemoteException;

  /**
   * Permet de signaler à l'entité qu'elle peut s'arrêter
   *
   * Utilisé à la fin du jeu
   */
  public void stop() throws RemoteException;

  /** Initialise l'entité avec la configuration donnée
   * 
   * @param vConfig : configuration à appliquer
   * @throws RemoteException 
   */
  public void init(Configuration vConfig) throws RemoteException;
  
  /** Méthode appelée par le jeu pour signaler le début du tour
   * 
   * @throws RemoteException 
   */
  public void lancerTour() throws RemoteException;

  /** Signale à l'entité que le tour courrant du jeu est terminé 
   *
   * @throws RemoteException
   */
  public void signalerFinTour() throws RemoteException;
  
  /** Renvoie l'état actuel de l'entité (stock)
   * @return l'état de l'entité
   * @throws java.rmi.RemoteException
   */
  public String etat() throws RemoteException;


  /** Retourne le nom de l'entité avec son id inclus
   *
   * @return le nom de l'entité
   * @throws java.rmi.RemoteException
   */
  public String nom() throws RemoteException;

  /** Méthode appelée lorsqu'un agent récolte une ressource
   * (basée sur la configuration du jeu)
   *   
   * @param vQuantitéRecolte : la quantité demandée
    * @return la valeur déduite du stock
    * @throws java.rmi.RemoteException
  */
  public int reduireStock(int vQuantitéRecolte) throws RemoteException;
  

}
