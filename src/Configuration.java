/**
 * Joseph Stich
 * Kevin Masson
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */

import java.util.Map;
import java.util.HashMap;
import java.io.*;

/** Représente une configuration utilisé pour le jeu
 */
public class Configuration implements Serializable{

  private static final long serialVersionUID = 41L;

  /** Réglages liés à la configuration */
  private HashMap<String, Integer> params;

  /** Constructeur par défaut
   */
  public Configuration(){

    params = new HashMap<>();

    // Paramètres par défaut
    
    params.put("init", 0); // Permet de savoir si demarrerConfiguration a été appellé ou non

    // Producteurs
    params.put("frequenceProduction", 1);
    params.put("stockProdInitial", 100);
    params.put("stockProdMax", -1);
    params.put("evolutionProduction", 2);
    params.put("quantiteProduction", 10);

    // Agents
    params.put("recolte", 1);
    params.put("evolutionRecolte", 2);
    params.put("quantiteRecolte" , params.get("stockProdInitial") / 5);
    params.put("volAgent", 0);
    params.put("agentControlable", 0);
    params.put("stockVictoireAgent", 100);

    // Jeu
    params.put("conditionVictoire", 3);
    params.put("deroulement", 1);
    params.put("parties", 1);

  }

  /** Règle le jeu en se basant sur un fichier de config
   *
   * @param fichier   Le fichier de configuration
   */
  public void demarrerConfiguration(String fichier) throws ClassNotFoundException, IOException{

    File file = new File(fichier);
    BufferedReader reader = new BufferedReader(new FileReader(file));

    String line;
    String split[];
    while ((line = reader.readLine()) != null) {
      split = line.split(" ");
      try{
        params.put(split[0], Integer.parseInt(split[1]));
      }catch(Exception e){
        throw new IOException("Impossible de parser la configuration ("
            + line + ").");
      }
    }

    params.put("init", 1);
    // TO DO
  }

  /** Permet à l'utilisateur de régler le jeu
   */
  public void demarrerConfiguration(){
    params.put("init", 1);
    int choix;
    choix = Utils.choixTerminal("Voulez utiliser la configuration par défaut ?", new String[]{"y", "n"}, 0);

    if(choix == 0) return;

    // Choix stock prod initial
    choix = Utils.choixTerminalInt("Quantité de ressources initial pour les producteurs :", 
		    params.get("stockProdInitial"), 
		    conditionTerm.Num.POSITIVEOUNULLE);


    params.put("stockProdInitial", choix);

    // Choix stock prod max
    choix = Utils.choixTerminalInt("Quantité de ressources maximales pour les producteurs :", 
		    params.get("stockProdMax"), 
		    conditionTerm.Num.NONNULLE);
    params.put("stockProdMax", choix);

    // Choix evolution recolte
    choix = Utils.choixTerminal("Evolution de la production des ressources :", 
		    new String[]{
		    "Augmente de la moitié du stock initial + 1"
		    , "Augmente d'une valeur fixe"
		    , "Augmente de plus en plus à chaque tour"}
		    , 1);


    params.put("evolutionProduction", choix + 1); // Les index sont offset

    if(choix == 1){ // Choix augmentation fixe prod
	    choix = Utils.choixTerminalInt("Quantité fixe de ressources produites :", 
			    params.get("quantiteProduction"), 
			    conditionTerm.Num.POSITIVE);
	    params.put("quantiteProduction", choix);
    }

    // Choix evolution recolte

    choix = Utils.choixTerminal("Evolution de la recolte des ressources :", 
		    new String[]{
		    "La quantité de ressources recoltés par un agent est fixe"
		    , "La quantité recoltée est un pourcentage du stock initial de production"}
		    , 1);


    params.put("evolutionRecolte", choix + 1); // Les index sont offset

    if(choix == 1){ // Choix du pourcentage
	    choix = Utils.choixTerminalInt("Pourcentage de ressources recoltées (90 max):",
			    20,
			    conditionTerm.Num.POSITIVE);
	    if(choix > 90) choix = 90;
	    params.put("quantiteRecolte", params.get("stockProdInitial") / (100/choix));
    }
    choix = Utils.choixTerminalInt("Stock requis pour la victoire d'un agent (>0):",
		    params.get("stockVictoireAgent"),
		    conditionTerm.Num.POSITIVE);

    params.put("stockVictoireAgent", choix);

    System.out.println(params);


  }

  /** Permet de récuperer un paramètre
   *
   * Si la cléf n'existe pas, une exception est lancée
   *
   * @param key   Nom du paramètre
   * @return      Un entier représentant la valeur du paramètre
   * @throws NullPointerException   Lorsque la cléf demandée n'existe pas
   */
  public int getParam(String key) throws NullPointerException{
	  if(!params.containsKey(key)) throw new NullPointerException("Aucun paramètre n'est associé à cette cléf");

	  return params.get(key);


  }

  /**
   * Permet d'enrigster la configuration dans un fichier
   *
   * @param filename  Le fichier dans lequel il faut enregistrer la conf
   * @throws IOException
   */
  public void save(String filename) throws IOException{
    FileWriter fstream;
    BufferedWriter out;

    fstream = new FileWriter(filename);
    out = new BufferedWriter(fstream);

    for(Map.Entry<String, Integer> entry : params.entrySet()){

      out.write(entry.getKey() + " " + entry.getValue() + "\n");

    }

    out.close();

  }
}
