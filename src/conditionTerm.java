/**
 * Joseph Stich
 * Kevin Masson
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */

/** 
 * Condtions utilisés pour les valeurs entrées par l'utilisateur dans le terminal
 */
public class conditionTerm{

  /** Conditions utilisés pour les choix de valeur numérique
  */
  public enum Num{
    POSITIVE, NEGATIVE, NULLE, POSITIVEOUNULLE, NEGATIVEOUNULLE, ALL, NONNULLE
  }

}
